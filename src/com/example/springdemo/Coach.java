package com.example.springdemo;

public interface Coach {
	
	public String getDailyWorkOuts();
	public String getDailyFortune();
	
	
}
