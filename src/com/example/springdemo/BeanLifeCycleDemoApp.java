package com.example.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanLifeCycleDemoApp {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context1=new ClassPathXmlApplicationContext("beanLifeCycle-applicationContext.xml");
		
		Coach theCoach=context1.getBean("myCoach",TrackCoach.class);
		
		System.out.println(theCoach.getDailyWorkOuts());
		
		context1.close();
		
	}

}
