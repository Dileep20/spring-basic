package com.example.springdemo;

public class BaseballCoach implements Coach{
	//define private field for dependency
	private FortuneService thefortuneService;
	
	//define constructor for dependency injection
	public BaseballCoach(FortuneService fortuneService) {
		thefortuneService=fortuneService;
		//this is child branch
		System.out.println("this is child branch");
		System.out.println("Bug Fixed Twice");
	}
	
	public String getDailyWorkOuts() {
		return "batting ";
	}

	@Override
	public String getDailyFortune() {
		return thefortuneService.getFortune();
	}
	public void testMerge() {
		System.out.println("this method is created yto test merging ");
	}
	
	
}
