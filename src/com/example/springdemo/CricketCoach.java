package com.example.springdemo;

public class CricketCoach implements Coach{

	private FortuneService fortuneService;
	//add new filelds
	private String emailAddress;
	private String team;
	
	public String getEmailAddress() {
		return emailAddress;
		
	}

	public void setEmailAddress(String emailAddress) {
		System.out.println("Cricket Coach : email address");
		this.emailAddress = emailAddress;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		System.out.println("Cricket Coach : set Team");
		this.team = team;
	}

	//creating no-arg constructor
	public CricketCoach() {
		System.out.println("No-Arg constructor");
	}
	
	public void setFortuneService(FortuneService fortuneservice) {
		System.out.println("setter method .....");
		this.fortuneService = fortuneservice;
	}

	@Override
	public String getDailyWorkOuts() {
		// TODO Auto-generated method stub
		return "still working out !!!";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}

}
