package com.example.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {

	public static void main(String[] args) {
		//loading spring configuration file
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//retrieve bean from spring container
		Coach theCoach=context.getBean("myCoach",Coach.class);
		//call methods on bean
		System.out.println(theCoach.getDailyWorkOuts());
		
		System.out.println(theCoach.getDailyFortune());
		//close the context
		context.close();
	}
	
	
}
