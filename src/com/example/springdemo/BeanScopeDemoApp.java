package com.example.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScopeDemoApp {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context1=new ClassPathXmlApplicationContext("beanScope-applicationContext.xml");
		
		Coach theCoach=context1.getBean("myCoach",TrackCoach.class);
		
		Coach theCoach1=context1.getBean("myCoach",TrackCoach.class);
		
		boolean ref=theCoach==theCoach1;
		System.out.println(ref);
		
		System.out.println(theCoach);
		
		System.out.println(theCoach1);
		
		context1.close();
		
	}

}
