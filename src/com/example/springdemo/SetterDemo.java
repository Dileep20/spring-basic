package com.example.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterDemo {

	public static void main(String[] args) {
		//load spring configuration file
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		
		
		//retrieve bean from spring container
		Coach thecoach=context.getBean("myCricketCoach",CricketCoach.class);
		
		
		
		//call methods on the bean
		System.out.println(thecoach.getDailyWorkOuts());
		System.out.println(thecoach.getDailyFortune());
		System.out.println(((CricketCoach) thecoach).getEmailAddress());
		System.out.println(((CricketCoach) thecoach).getTeam());
		//close context
		context.close();
	}

}
