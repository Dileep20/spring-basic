package com.example.springdemo;

public class TrackCoach implements Coach {
	
	private FortuneService fortuneService;
	
	public TrackCoach(FortuneService fortune) {
		this.fortuneService=fortune;
	}
	public TrackCoach() {
		
	}
	
	@Override
	public String getDailyWorkOuts() {
		
		return "Run for 5kms";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
	
	//add init method
	public void doMyStartupStuff() {
		System.out.println("Track Coach : This is start up Method ");
	}
	
	//add destroy method
	public void doMyCleanupStuff() {
		System.out.println("Track Coach : This is cleanu up Method ");
	}
	
	
	

}
